const mix = require('laravel-mix');

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

mix.js('resources/assets/js/app.js', 'public/assets/js/app.js')
   .postCss('resources/assets/css/app.css', 'public/assets/css/app.css')
   .tailwind('./tailwind.config.js')
   .purgeCss();

if (mix.inProduction()) {
  mix.sourceMaps().version();
}
