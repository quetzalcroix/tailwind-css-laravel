# **Tailwind CSS - Laravel**

Bootstrapping Laravel 5.8 with tailwindcss.

Just clone and install packages

`yarn install && yarn run dev`

or

`npm install && npm run dev`

## Software Used:

[**Laravel 5.8**](https://laravel.com)

[**Tailwind CSS**](https://tailwindcss.com)

### Credits
[Laravel Frontend Tailwind CSS](https://github.com/laravel-frontend-presets/tailwindcss)
