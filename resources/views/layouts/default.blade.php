<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - {{ config('app.name', 'Quetzalcroix') }}</title>
    <link href="{{ mix('assets/css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body class="bg-gray-100 h-screen antialiased leading-none">

@include('components.navbar')

<div class="flex flex-wrap">
    @yield('content')
</div>

@include('components.footer')

<script src="{{ mix('assets/js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
