@extends('layouts.default')

@section('title','Home')

@section('content')
    <div class="w-1/5">

    </div>
    <div class="w-3/5 text-center">
        <h1 class="text-5xl text-gray-800">Welcome</h1>
        <h2 class="text-3xl text-gray-900">Laravel & TailwindCSS</h2>
    </div>
    <div class="w-1/5">

    </div>
@stop
