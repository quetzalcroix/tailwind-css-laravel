<ul class="flex flex-wrap justify-between py-3 bg-blue-100">
    <li class="ml-20">
        <a class="inline-block border rounded py-2 px-4 bg-blue-300 hover:bg-blue-500 text-white"
           href="{{ route('index') }}">Home</a>
    </li>
    <li class="mr-20">
        <ul class="flex flex-wrap">
            <li class="mx-1">
                <a class="inline-block border border-blue-300 rounded-full py-2 px-4 bg-blue-300 hover:bg-blue-500 text-white"
                   href="#">Login</a>
            </li>

            <li class="mx-1">
                <a class="inline-block border border-blue-300 rounded-full py-2 px-4 bg-blue-300 hover:bg-blue-500 text-white"
                   href="#">Register</a>
            </li>
        </ul>
    </li>
</ul>
